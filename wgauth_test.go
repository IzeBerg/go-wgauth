package wgauth

import (
	"os"
	"testing"
)

func TestDoAuth(t *testing.T) {
	twoFactor := &TwoFactorInput{os.Getenv(`otp`), os.Getenv(`backup`)}
	if token, raw, err := RealmRU.DoAuth(``, os.Getenv(`username`), os.Getenv(`password`), twoFactor); err != nil {
		t.Fatal(raw, err)
	} else if token.ErrorDescription != `` {
		t.Fatal(raw, token)
	} else {
		if token, raw, err := token.Exchange(``); err == nil {
			if token.ErrorDescription != `` {
				t.Fatal(token)
			} else {
				if _, raw, err := token.CredentialsToken(`token1`, `wot`); err != nil {
					t.Fatal(raw, err)
				}
				if data, raw, err := token.AccountInfo(`id`, `nickname`, `email`, `country_legal`); err != nil {
					t.Fatal(raw, err)
				} else {
					t.Log(raw, data)
				}
			}
		} else {
			t.Fatal(raw, err)
		}
	}
}

func TestChallengeResults_ProcessPOW(t *testing.T) {
	res := ChallengeResults{}
	res.POW.Timestamp = 1561345761
	res.POW.Type = `pow`
	res.POW.RandomString = `GTrtzdfb8R9ZNQUh`
	res.POW.Complexity = 3
	res.POW.Algorithm.Version = 1
	res.POW.Algorithm.Resourse = `wgni`
	res.POW.Algorithm.Name = `hashcash`
	if pow, err := res.ProcessPOW(); pow != 4395 || err != nil {
		t.Fatal(`unexpected pow`, pow, err)
	}
}
