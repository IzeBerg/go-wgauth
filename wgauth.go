package wgauth

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"golang.org/x/crypto/sha3"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	challengeOAuthURL = `/id/api/v2/account/credentials/create/oauth/token/challenge/`
	tokenOAuthURL     = `/id/api/v2/account/credentials/create/oauth/token/`
	tokenURL          = `/id/api/v2/account/credentials/create/` // + tokentype
	accountInfoURL    = `/id/api/v2/account/info/`
)

var (
	RealmRU   = RealmConfig{`ru.wargaming.net`, `77cxLwtEJ9uvlcm2sYe4O8viIIWn1FEWlooMTTqF`, `wgc/19.03.00.5220`}
	RealmEU   = RealmConfig{`eu.wargaming.net`, `JJ5yuABVKqZekaktUR8cejMzxbbHAtUVmY2eamsS`, `wgc/19.03.00.5220`}
	RealmNA   = RealmConfig{`na.wargaming.net`, `AJ5PLrEuz5C2d0hHmmjQJtjaMpueSahYY8CiswHE`, `wgc/19.03.00.5220`}
	RealmASIA = RealmConfig{`asia.wargaming.net`, `Xe2oDM8Z6A4N70VZIV8RyVLHpvdtVPYNRIIYBklJ`, `wgc/19.03.00.5220`}
)

type RealmConfig struct {
	WGNIHost, ClientID, UserAgent string
}

type ChallengeResults struct {
	Cookies []*http.Cookie
	POW     struct {
		Timestamp    int64  `json:"timestamp"`
		Type         string `json:"type"`
		Complexity   int64  `json:"complexity"`
		RandomString string `json:"random_string"`
		Algorithm    struct {
			Resourse  string `json:"resourse"`
			Name      string `json:"name"`
			Extension string `json:"extension"`
			Version   int64  `json:"version"`
		} `json:"algorithm"`
	} `json:"pow"`

	// TODO: captcha?
}

//ProcessPOW resolves Proof of Work challenge
func (s ChallengeResults) ProcessPOW() (int, error) {
	prefix := ``
	for i := int64(0); i < s.POW.Complexity; i++ {
		prefix += `0`
	}
	switch s.POW.Algorithm.Name {
	case `hashcash`:
		baseStr := stringsJoin(
			`:`,
			strconv.FormatInt(s.POW.Algorithm.Version, 10),
			strconv.FormatInt(s.POW.Complexity, 10),
			strconv.FormatInt(s.POW.Timestamp, 10),
			s.POW.Algorithm.Resourse,
			s.POW.Algorithm.Extension,
			s.POW.RandomString,
			``, // stub for iter
		)

		var i int
		for {
			hashStr := baseStr + strconv.Itoa(i)
			hash := sha3.NewLegacyKeccak512()
			_, _ = io.WriteString(hash, hashStr)
			digest := hex.EncodeToString(hash.Sum(nil))
			if digest[:s.POW.Complexity] == prefix {
				return i, nil
			}
			i++
		}
	default:
		return 0, Error{`unknown pow algorithm`}
	}
}

type Error struct {
	Message string
}

func (s Error) Error() string {
	return s.Message
}

type AuthTokenResult struct {
	// Done
	AccessToken string        `json:"access_token"`
	IdToken     string        `json:"id_token"`
	ExpiresIn   time.Duration `json:"expires_in"`
	TokenType   string        `json:"token_type"`
	User        int64         `json:"user"`
	Scope       string        `json:"scope"`

	// 2FA
	TwoFactorTypes []string `json:"twofactor_types"`
	TwoFactorToken string   `json:"twofactor_token"`

	// Errors (includes 2FA required)
	ErrorDescription string      `json:"error_description"`
	ErrorCodes       interface{} `json:"errors_codes"`
	Error            string      `json:"error"`

	// internal
	ExchangeCode string
	Realm        RealmConfig
}

type TwoFactorInput struct {
	OTP, Backup string
}

//Challenge requests challenge, see DoAuth
func (s RealmConfig) Challenge() (*ChallengeResults, string, error) {
	reqURL := s.getWGNIMethod(challengeOAuthURL)
	if req, err := s.getRequest(`GET`, reqURL.String(), nil, nil); err == nil {
		res := &ChallengeResults{}
		if resp, raw, err := s.doRequest(req, res); err == nil {
			defer resp.Body.Close()
			res.Cookies = resp.Cookies()
			return res, raw, nil
		} else {
			return nil, raw, err
		}
	} else {
		return nil, ``, err
	}
}

//AuthBasic requests to basic auth
func (s RealmConfig) AuthBasic(trackingID, username, password, twofactorToken string, twofactor *TwoFactorInput, pow int, cookies []*http.Cookie) (*AuthTokenResult, string, error) {
	reqURL := s.getWGNIMethod(tokenOAuthURL)
	form := url.Values{}
	form.Set(`username`, username)
	form.Set(`password`, password)
	form.Set(`grant_type`, `urn:wargaming:params:oauth:grant-type:basic`)
	form.Set(`client_id`, s.ClientID)
	form.Set(`tid`, trackingID)
	form.Set(`pow`, strconv.Itoa(pow))

	if twofactorToken != `` {
		if twofactor.OTP != `` {
			form.Set(`otp_code`, twofactor.OTP)
		} else if twofactor.Backup != `` {
			form.Set(`backup_code`, twofactor.OTP)
		} else {
			panic(Error{`empty TwoFactorInput`})
		}
		form.Set(`twofactor_token`, twofactorToken)
	}
	result := &AuthTokenResult{Realm: s}
	raw, err := s.doRequestWithLocation(reqURL, form, cookies, nil, result)
	return result, raw, err
}

//AuthToken requests to exchange the access token
func (s RealmConfig) AuthToken(trackingID, accessToken string) (*AuthTokenResult, string, error) {
	exchangeCodeBytes := make([]byte, 16)
	rand.Read(exchangeCodeBytes)
	result := &AuthTokenResult{Realm: s, ExchangeCode: hex.EncodeToString(exchangeCodeBytes)}

	reqURL := s.getWGNIMethod(tokenOAuthURL)
	form := url.Values{}
	form.Set(`access_token`, accessToken)
	form.Set(`grant_type`, `urn:wargaming:params:oauth:grant-type:access-token`)
	form.Set(`client_id`, s.ClientID)
	form.Set(`exchange_code`, result.ExchangeCode)
	form.Set(`tid`, trackingID)

	raw, err := s.doRequestWithLocation(reqURL, form, nil, nil, result)
	return result, raw, err
}

//Exchange the current OAuth token to new OAuth token
func (s AuthTokenResult) Exchange(trackingID string) (*AuthTokenResult, string, error) {
	return s.Realm.AuthToken(trackingID, s.AccessToken)
}

//CredentialsToken requests the credentials token, known tokens:
// token1 - it allows to login into game client
func (s AuthTokenResult) CredentialsToken(tokenType, requestedFor string) (map[string]interface{}, string, error) {
	reqURL := s.Realm.getWGNIMethod(tokenURL + tokenType + `/`)
	form := url.Values{`requested_for`: {requestedFor}}
	result := map[string]interface{}{}
	raw, err := s.Realm.doRequestWithLocation(reqURL, form, nil, &s, &result)
	return result, raw, err
}

//AccountInfo requests common account info, by default WGNI service allows to request fields:
// id, nickname, email, country_legal
func (s AuthTokenResult) AccountInfo(fields ...string) (map[string]interface{}, string, error) {
	reqURL := s.Realm.getWGNIMethod(accountInfoURL)
	form := url.Values{`fields`: {stringsJoin(`,`, fields...)}}
	result := map[string]interface{}{}
	raw, err := s.Realm.doRequestWithLocation(reqURL, form, nil, &s, &result)
	return result, raw, err
}

//DoAuth auth process
func (s RealmConfig) DoAuth(trackingID, username, password string, twofactor *TwoFactorInput) (*AuthTokenResult, string, error) {
	// Request challenge
	if res, raw, err := s.Challenge(); err == nil {
		if pow, err := res.ProcessPOW(); err == nil { // Resolve Proof of Work
			// Do basic auth without 2FA
			if token, raw, err := s.AuthBasic(trackingID, username, password, ``, nil, pow, res.Cookies); err == nil {
				if token.ErrorDescription == `twofactor_required` && twofactor != nil {
					// if 2FA required and specified - do basic auth with 2FA
					return s.AuthBasic(trackingID, username, password, token.TwoFactorToken, twofactor, pow, res.Cookies)
				} else {
					return token, raw, err
				}
			} else {
				return nil, raw, err
			}
		} else {
			return nil, raw, err
		}
	} else {
		return nil, raw, err
	}
}

func (s RealmConfig) getWGNIMethod(method string) url.URL {
	return url.URL{
		Scheme: `https`,
		Host:   s.WGNIHost,
		Path:   method,
	}
}

func (s RealmConfig) getRequest(httpMethod, url string, body interface{}, cookies []*http.Cookie) (*http.Request, error) {
	var data io.Reader
	if body != nil {
		switch body.(type) {
		case []byte:
			data = bytes.NewBuffer(body.([]byte))
			break
		case string:
			data = bytes.NewBuffer([]byte(body.(string)))
			break
		default:
			if d, err := json.Marshal(body); err == nil {
				data = bytes.NewBuffer(d)
			} else {
				return nil, err
			}
			break
		}
	}
	if req, err := http.NewRequest(httpMethod, url, data); err == nil {
		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}
		return req, nil
	} else {
		return nil, err
	}
}

func (s RealmConfig) doRequest(req *http.Request, result interface{}) (*http.Response, string, error) {
	req.Header.Set(`User-Agent`, s.UserAgent)
	if resp, err := http.DefaultClient.Do(req); err == nil {
		var data []byte
		if d, err := ioutil.ReadAll(resp.Body); err == nil {
			data = d
		} else {
			return resp, ``, err
		}

		if result != nil {
			defer resp.Body.Close()
			if err := json.Unmarshal(data, result); err == nil {
				return resp, string(data), nil
			} else {
				return resp, string(data), err
			}
		} else {
			return resp, string(data), nil
		}
	} else {
		return nil, ``, err
	}
}

func (s RealmConfig) doRequestWithLocation(reqURL url.URL, form url.Values, cookies []*http.Cookie, auth *AuthTokenResult, result interface{}) (string, error) {
	if req, err := s.getRequest(`POST`, reqURL.String(), form.Encode(), cookies); err == nil {
		req.Header.Set(`Content-Type`, `application/x-www-form-urlencoded`)

		if auth != nil {
			token := auth.AccessToken
			if auth.ExchangeCode != `` {
				token += `:` + auth.ExchangeCode
			}
			req.Header.Set(`Authorization`, auth.TokenType+` `+token)
		}

		if resp, raw, err := s.doRequest(req, nil); err == nil {
			defer resp.Body.Close()
			if location := resp.Header.Get(`Location`); location != `` {
				if req, err := s.getRequest(`GET`, location, nil, cookies); err == nil {
					if _, raw, err := s.doRequest(req, result); err == nil {
						return raw, nil
					} else {
						return raw, err
					}
				} else {
					return raw, err
				}
			} else {
				return raw, Error{raw}
			}
		} else {
			return raw, err
		}
	} else {
		return ``, err
	}
}

func stringsJoin(sep string, strs ...string) string {
	return strings.Join(strs, sep)
}
